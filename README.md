# **Rasty OS kernel**

A kernel of my OS. I'm making the OS to learn some more about OS development and to learn more about programming in general especially in [Rust](https://www.rust-lang.org)

## **Sources**
* [Philipp Oppermann's blog](https://os.phil-opp.com)
* [OSDev wiki](https://wiki.osdev.org)
* [xv6](https://pdos.csail.mit.edu/6.828/2014/xv6.html)

## **My idea**
>_This will probably change a lot._

I would like to make unix-like OS. Mainly I like the idea of everything is a file. But the purpose of the OS is to learn more about operating systems and how they work so it may take a while before I'm able to do that.

## **Progress**
* **BARE BONES**
  * [x] A Freestanding Rust Binary
  * [x] A Minimal Rust Kernel
  * [x] VGA Text Mode
  * [x] Testing
* **EXCEPTIONS**
  * [x] CPU Exceptions
  * [x] Double Faults
  * [x] Hardware Interrupts
* **MEMORY MANAGEMENT**
  * [x] Introduction to Paging
  * [x] Paging Implementation
  * [x] Heap Allocation
* **More research**
  * [x] Figure out what next
  * [ ] Read xv6 book
  * [ ] Implement what I learned
    * [ ] Syscalls
    * [ ] File System
    * [ ] Process scheduling
    * [ ] ...